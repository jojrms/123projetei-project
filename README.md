# SOBRE O PROJETO
Este código simula uma área de login, com o acesso pré-definido, onde, caso dê sucesso, direciona o usuário a um dashboard, em que poderá visualizar usuários fakes já existentes, criar, editar e excluir. 

O mockup utilizado não foi criado pela desenvolvedora, mas retirado do site Dribbble. Os créditos serão atribuidos mais abaixo, no setor MOCKUP. Algumas alterações foram realizadas.

### PÁGINA DE LOGIN
email: 123projetei@gmail.com
senha: 123projetei

## REQUISIÇÕES
React = ^18.2.0,
React-redux = ^8.0.4,
Node = >=6.0.0

## LINGUAGENS, FRAMEWORKS E BIBLIOTECAS
Linguagem: JavaScript
Framework: React.js
Bibliotecas: Redux

## MOCKUP
O mockup usado foi retirado do site Dribbble.
Atualmente o design não se encontra de forma responsiva, tendo sido modelado apenas para desktop. As unidades utilizadas foram, em sua grande maioria, "vh", "vw" e "%".

### LOGIN (MOCKUP)
Usuário: Jordan Hughes
Link: https://dribbble.com/shots/19338138-Log-in-page-Untitled-UI

### DASHBOARD (MOCKUP)
Usuário: Budiarti R.
Link: https://dribbble.com/shots/18882866-Virtual-School-Dashboard-Design

### LOGOTIPO (MOCKUP)
Usuário: Vito Arvy
Link: https://dribbble.com/shots/19274765-Rooom-Logo-Design

## COMO INICIAR
1. Clone este repositório no CMD utilizando o comando "git clone URL_AQUI",
2. Após finalizado, entre no arquivo e abra em seu editor de código-fonte,
3. Realize um npm install para baixar todos os módulos node,
4. Finalize com um 'npm start' para abrir um localhost